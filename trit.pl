:- [bucket/consult].
:- [debug/consult].
:- [generator/generator].
:- [kolekcjoner/kolekcjoner].
:- [policer/consult].
:- [sch/consult].

sumuj(Xs, Ys) :-
	sumuj(0, Xs, Ys).
sumuj(_, [], []).
sumuj(S, [X|Xs], [Y|Ys]) :-
	Y is X+S,
	sumuj(Y, Xs, Ys).
pisz_do(P, L) :-
	tell(P),
	maplist(writeln, L),
	told.

przekszt(_, S, S, [], []).
przekszt(P, S, NS, [X|Xs], [Y|Ys]):-
	call(P, S, TS, X, Y),
	przekszt(P, TS, NS, Xs, Ys).

losuj_klase(P1, P2, stan, stan, T, pakiet(K, T, T)) :-
	R is random_float,
	%klase zmieniamy tak, jak stan
	zmien_stan(R, P1, P2, 0, 1, 2, K).
