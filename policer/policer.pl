policer(DT, K, S) :-
	policer(DT, 0, K, S).

policer(DT, T, K, S) :-
	thread_get_message(M),
	policer_wiad(DT, T, K, S, M).

policer_wiad(_, _, _, S, koniec) :-
	thread_send_message(S, koniec).
policer_wiad(DT, T, K, S, P) :-
	P = pakiet(_, _, Tp),
	Tp < T,
	thread_send_message(K, odrzucona),
	policer(DT, T, K, S).
policer_wiad(DT, T, K, S, P) :-
	P = pakiet(_, _, Tp),
	Tp >= T,
	thread_send_message(S, P),
	NT is Tp + DT,
	policer(DT, NT, K, S).

