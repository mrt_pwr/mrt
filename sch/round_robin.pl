:- [prior_lib].

round_robin(R, Dokad) :-
	inicjuj_prior(P),
	round_robin_redukuj(R, Dokad, nie, P, 0).

round_robin_redukuj(_, D, tak, F, 0) :-
	\+ niepuste_prior(F),
	!,
	thread_send_message(D, koniec).
round_robin_redukuj(R, D, B, F, Z) :-
	niepuste_prior(F),
	Z > 0,
	!,
	usun_z_prior(R, P, F, F1, L1, L2),
	thread_send_message(D, P),
	Z1 is Z-1,
	nowe_r(L1, L2, NR),
	round_robin_redukuj(NR, D, B, F1, Z1).
round_robin_redukuj(R, D, B, F, Z) :-
	thread_get_message(M),
	round_robin_wiad(R, D, B, F, Z, M).

round_robin_wiad(R, D, B, F, Z, zezwolenie) :-
	Z1 is Z+1,
	round_robin_redukuj(R, D, B, F, Z1).
round_robin_wiad(R, D, B, F, Z, P) :-
	P = pakiet(_, _, _),
	dodaj_prior(P, F, F1),
	round_robin_redukuj(R, D, B, F1, Z).
round_robin_wiad(R, D, _, F, Z, koniec) :-
	round_robin_redukuj(R, D, tak, F, Z).

nowe_r(X, Y, W) :-
	reverse(X, RX),
	append(Y, RX, W).
