:- [fifo_lib].

wymien(0, X, Y, prior(X, B, C), prior(Y, B, C)).
wymien(1, X, Y, prior(A, X, C), prior(A, Y, C)).
wymien(2, X, Y, prior(A, B, X), prior(A, B, Y)).

inicjuj_prior(prior(X-X, Y-Y, Z-Z)).

niepuste_prior(prior(A, B, C)) :-
	%Achtung, or!
	niepuste_fifo(A);
	niepuste_fifo(B);
	niepuste_fifo(C).

usun_z_jednej_prior(N, E, P, NP) :-
	wymien(N, K, NK, P, NP),
	niepuste_fifo(K),
	usun_fifo(E, K, NK).

usun_z_prior([H|T], Hs, E, P, NP, [H|Hs], T) :-
	usun_z_jednej_prior(H, E, P, NP), !.
usun_z_prior([H|T], Hs, E, P, NP, L1, L2) :-
	usun_z_prior(T, [H|Hs], E, P, NP, L1, L2).

usun_z_prior(L, E, P, NP, L1, L2) :-
	usun_z_prior(L, [], E, P, NP, L1, L2).

dodaj_prior(P, K, NK) :-
	P = pakiet(Klasa, _, _),
	wymien(Klasa, Kol, NKol, K, NK),
	dodaj_fifo(P, Kol, NKol).
