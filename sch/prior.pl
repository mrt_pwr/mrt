:- [prior_lib].

prior(Dokad) :-
	inicjuj_prior(P),
	prior_redukuj(Dokad, nie, P, 0).

prior_redukuj(D, tak, F, 0) :-
	\+ niepuste_prior(F),
	!,
	thread_send_message(D, koniec).
prior_redukuj(D, B, F, Z) :-
	niepuste_prior(F),
	Z > 0,
	!,
	usun_z_prior([0, 1, 2], P, F, F1, _, _),
	thread_send_message(D, P),
	Z1 is Z-1,
	prior_redukuj(D, B, F1, Z1).
prior_redukuj(D, B, F, Z) :-
	thread_get_message(M),
	prior_wiad(D, B, F, Z, M).

prior_wiad(D, B, F, Z, zezwolenie) :-
	Z1 is Z+1,
	prior_redukuj(D, B, F, Z1).
prior_wiad(D, B, F, Z, P) :-
	P = pakiet(_, _, _),
	dodaj_prior(P, F, F1),
	prior_redukuj(D, B, F1, Z).
prior_wiad(D, _, F, Z, koniec) :-
	prior_redukuj(D, tak, F, Z).
