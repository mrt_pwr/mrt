dodaj_fifo(E, X-[E|Y], X-Y).
niepuste_fifo(X-Y) :-
	not(
		not(
			(
				Y = [],
				X \= []))).
usun_fifo(E, F, X-Y) :-
	niepuste_fifo(F),
	F = [E|X]-Y.

dodaj_fifoP(E, F, NF):-
	E = pakiet(K, _, _),
	wymien(K, Q, _, F, _),
	dodaj_fifo(E, Q, NQ),
	wymien(K, _, NQ, F, NF).
