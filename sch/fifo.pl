:- [fifo_lib].

fifo(Dokad) :-
	fifo_redukuj(Dokad, nie, X-X, 0).

fifo_redukuj(D, tak, F, 0) :-
	\+ niepuste_fifo(F),
	!,
	thread_send_message(D, koniec).
fifo_redukuj(D, B, F, Z) :-
	niepuste_fifo(F),
	Z > 0,
	!,
	usun_fifo(P, F, F1),
	thread_send_message(D, P),
	Z1 is Z-1,
	fifo_redukuj(D, B, F1, Z1).
fifo_redukuj(D, B, F, Z) :-
	thread_get_message(M),
	fifo_wiad(D, B, F, Z, M).

fifo_wiad(D, B, F, Z, zezwolenie) :-
	Z1 is Z+1,
	fifo_redukuj(D, B, F, Z1).
fifo_wiad(D, B, F, Z, P) :-
	P = pakiet(_, _, _),
	dodaj_fifo(P, F, F1),
	fifo_redukuj(D, B, F1, Z).
fifo_wiad(D, _, F, Z, koniec) :-
	fifo_redukuj(D, tak, F, Z).
