leaky(DT, Dokad) :-
	thread_get_message(GdzieZezw),
	thread_send_message(GdzieZezw, zezwolenie),
	leaky_petla(DT, 0, GdzieZezw, Dokad).

leaky_petla(DT, T, Z, D) :-
	thread_get_message(M),
	leaky_wiad(DT, T, Z, D, M).

leaky_wiad(_, _, _, D, koniec) :-
	thread_send_message(D, koniec).
leaky_wiad(DT, T, Z, D, pakiet(A, B, C)) :-
	wieksze(T, C, NC),
	thread_send_message(D, pakiet(A, B, NC)),
	thread_send_message(Z, zezwolenie),
	T1 is T + DT,
	wieksze(T1, C, NT),
	leaky_petla(DT, NT, Z, D).

wieksze(X, Y, X) :-
	X > Y.
wieksze(X, Y, Y) :-
	X =< Y.
