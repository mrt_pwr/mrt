token(D, Vel, Vol) :-
	thread_get_message(Z),
	thread_send_message(Z, zezwolenie),
	token_czekaj(D, Z, 0, Vel, Vol, 0).

token_czekaj(D, Z, TT, Vel, Vol, N) :-
	thread_get_message(M),
	token_wiad(D, Z, TT, Vel, Vol, N, M).

token_wiad(D, _, _, _, _, _, koniec) :-
	thread_send_message(D, koniec).
token_wiad(D, Z, TT, Vel, Vol, N, P) :-
	P = pakiet(_, _, T),
	N1 is max(
		0,
		min(
			Vol,
				N
			+
				floor(
						(T-TT)
					/
						Vel))),
	NTT is max(
		TT,
			floor(T/Vel)
		*
			Vel),
	token_wyslij(D, Z, NTT, Vel, Vol, N1, P).

token_wyslij(D, Z, TT, Vel, Vol, 0, pakiet(K, T1, _)) :-
	NTT is TT + Vel,
	thread_send_message(D, pakiet(K, T1, NTT)),
	thread_send_message(Z, zezwolenie),
	token_czekaj(D, Z, NTT, Vel, Vol, 0).
token_wyslij(D, Z, TT, Vel, Vol, N, P) :-
	N > 0,
	N1 is N-1,
	thread_send_message(D, P),
	thread_send_message(Z, zezwolenie),
	token_czekaj(D, Z, TT, Vel, Vol, N1).
