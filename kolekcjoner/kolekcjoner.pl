kolekcjonuj(D) :-
	kolekcjonuj(D, []).

kolekcjonuj(D, L) :-
	thread_get_message(M),
	kolekcjonuj(D, L, M).

kolekcjonuj(D, L, koniec) :-
	!,
	thread_send_message(D,L).
kolekcjonuj(D, T, H) :-
	kolekcjonuj(D, [H|T]).

