% generator(parametry, stan/n, nowyStan/n, czas)

rand_exp(L, stan, stan, X) :-
	X is log(1 - random_float) / -L.

poisson(X, _, S, U, _, X) :-
	S >= U.
poisson(X, P, S, U, L, W) :-
	U > S,
	X1 is X+1,
	P1 is P * L / X1,
	S1 is S + P1,
	poisson(X1, P1, S1, U, L, W).

poisson(L, stan, stan, W) :-
	P is exp(-L),
	U is random_float,
	poisson(0, P, P, U, L, W).

zmien_stan(P1, P2, S1, S2, S3, S) :-
	R is random_float,
	zmien_stan(R, P1, P2, S1, S2, S3, S).

zmien_stan(R, P1, _, S1, _, _, S1) :-
	R < P1.
zmien_stan(R, P1, P2, _, S2, _, S2) :-
	R >= P1,
	R < P1+P2.
zmien_stan(R, P1, P2, _, _, S3, S3) :-
	R >= P1+P2.

generuj(_, _, 0, W) :-
	thread_send_message(W,koniec).
generuj(P, S, N, W) :-
	N > 0,
	N1 is N-1,
	call(P, S, NS, E),
	thread_send_message(W, E),
	generuj(P, NS, N1, W).

nrazy(N) :-
	N > 0.
nrazy(N) :-
	N > 0,
	N1 is N-1,
	nrazy(N1).

nowy_czas(on, _, 0).
nowy_czas(off, X, X).

on_off(L, P1, P2, stan(off, N), SW, W) :-
	zmien_stan(0, P2, inny, on, off, S),
	rand_exp(L, stan, stan, Przerwa),
	NN is N + Przerwa,
	on_off(L, P1, P2, stan(S, NN), SW, W).
on_off(L, P1, _, stan(on, N), stan(S, 0), W) :-
	zmien_stan(0, P1, inny, off, on, S),
	rand_exp(L, stan, stan, T),
	W is T+N.

mmpp(L1, _, _, P1, _, _, _, stan(1), stan(S), W) :-
	zmien_stan(0, P1, inny, 2, 1, S),
	rand_exp(L1, stan, stan, W).
mmpp(_, L2, _, _, P21, P23, _, stan(2), stan(S), W) :-
	zmien_stan(P21, P23, 1, 3, 2, S),
	rand_exp(L2, stan, stan, W).
mmpp(_, _, L3, _, _, _, P3, stan(3), stan(S), W) :-
	zmien_stan(0, P3, inny, 2, 3, S),
	rand_exp(L3, stan, stan, W).
